/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_file.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: csellier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 16:27:22 by csellier          #+#    #+#             */
/*   Updated: 2016/02/10 13:51:48 by csellier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_pix		*ft_check_file(char *chaine)
{
	t_pix			*tab;
	short int		size;

	if (chaine == NULL)
		return (NULL);
	if (ft_char(chaine) == -1)
		return (NULL);
	if ((size = ft_size(chaine)) == -1)
		return (NULL);
	if ((tab = (t_pix *)malloc(sizeof(t_pix) * (size + 1))) == NULL)
		return (NULL);
	tab[size].x = -32000;
	if (ft_fill_tab(chaine, tab, 0) == -1)
		return (NULL);
	return (tab);
}

int			ft_char(char *fichier)
{
	char	*buf;
	int		i;
	int		j;
	int		fd;

	if ((fd = open(fichier, O_RDONLY)) == -1)
		return (-1);
	if ((buf = ft_strnew(8)) == NULL)
		return (-1);
	while ((j = read(fd, buf, 8)) > 0)
	{
		i = 0;
		if (j == -1)
			return (-1);
		while (buf[i] != '\0')
		{
			if (!ft_isdigit(buf[i]) && buf[i] != ' ' && buf[i] != '\n' &&
					buf[i] != '-')
				return (-1);
			i++;
		}
		ft_strclr(buf);
	}
	free(buf);
	return (close(fd) + 1);
}

short int	ft_size(char *chaine)
{
	short int	i;
	int			fd;
	char		*buf;
	char		**tab;
	int			size;

	size = 0;
	i = 0;
	if (chaine == NULL || (fd = open(chaine, O_RDONLY)) == -1)
		return (-1);
	while (get_next_line(fd, &buf) == 1)
	{
		if ((tab = ft_strsplit(buf, ' ')) == NULL)
			return (-1);
		i += ft_tablen(tab);
		if (size < ft_tablen(tab))
			size = ft_tablen(tab);
		ft_free_tab(tab, NULL);
		free(buf);
	}
	if (close(fd) == -1)
		return (-1);
	return (i);
}

int			ft_fill_tab(char *file, t_pix *tab, int index)
{
	t_easy	z;

	z.y = 0;
	z.split = NULL;
	if (file == NULL || (z.fd = open(file, O_RDONLY)) == -1 || tab == NULL)
		return (-1);
	while (get_next_line(z.fd, &z.buffer) == 1)
	{
		z.split = ft_strsplit(z.buffer, ' ');
		z.x = -1;
		while (++z.x < ft_tablen(z.split))
		{
			tab[index].x = z.x;
			tab[index].y = z.y;
			tab[index].z = ft_atoi(z.split[z.x]);
			index++;
		}
		free(z.buffer);
		ft_free_tab(z.split, NULL);
		z.y++;
	}
	return (close(z.fd));
}

void		ft_free_tab(char **chaine, t_pix *tab)
{
	int		i;

	i = 0;
	if (chaine != NULL && tab == NULL)
	{
		while (chaine[i] != NULL)
		{
			free(chaine[i]);
			i++;
		}
		free(chaine);
	}
	else if (chaine == NULL && tab != NULL)
		free(tab);
	else
		return ;
}
