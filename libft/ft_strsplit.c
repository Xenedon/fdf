/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: csellier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/15 16:14:23 by csellier          #+#    #+#             */
/*   Updated: 2016/01/17 20:21:59 by csellier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_strsplit(char const *s, char c)
{
	unsigned int	i;
	size_t			j;
	int				k;
	char			**tab;

	i = 0;
	k = 0;
	tab = (char **)malloc(sizeof(char *) * (ft_countword(s, c) + 1));
	if (!tab || !s)
		return (NULL);
	while (s[i])
	{
		j = 0;
		while (s[i] == c && s[i])
			i++;
		j = i;
		while (s[i] != c && s[i])
			i++;
		if (j != i)
			tab[k++] = ft_strsub(s, j, (i - j));
	}
	tab[k] = NULL;
	return (tab);
}
