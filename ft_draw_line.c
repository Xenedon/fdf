/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: csellier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 22:20:21 by csellier          #+#    #+#             */
/*   Updated: 2016/02/10 14:28:49 by csellier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		ft_twin_index(t_pix *tab, int i)
{
	int ret;

	ret = -1;
	if (tab[i].y > 0)
	{
		ret = i - tab[i].x - 1;
		ret = ret - (tab[ret].x - tab[i].x);
	}
	if (tab[i].x != tab[ret].x)
		return (i);
	return (ret);
}

void	ft_colour(t_mlx *ptr, int i, int j, t_line *l)
{
	double	varcol;
	double	max;
	double	min;

	max = 0x00FF00;
	min = 0x0000FF;
	varcol = (max - min) / ptr->deltaz;
	if (ptr->tab[i].z < ptr->tab[j].z)
	{
		l->col1 = (ptr->tab[i].z - ptr->zmin) * varcol + min;
		l->col2 = (ptr->tab[j].z - ptr->zmin) * varcol + min;
	}
	else
	{
		l->col1 = (ptr->tab[j].z - ptr->zmin) * varcol + min;
		l->col2 = (ptr->tab[i].z - ptr->zmin) * varcol + min;
	}
}

void	ft_fill_pix(t_mlx *ptr, int i, int j, t_line *l)
{
	l->factor = 0.0;
	l->x0 = ptr->tab[i].xt;
	l->y0 = ptr->tab[i].yt;
	l->dx = ptr->tab[j].xt - ptr->tab[i].xt;
	l->dy = ptr->tab[j].yt - ptr->tab[i].yt;
}

int		ft_pixel_put(t_mlx *ptr, int i, int j, int k)
{
	t_line	l;

	if (k == 0)
		l.count = -1;
	ft_fill_pix(ptr, i, j, &l);
	if (k)
	{
		ft_colour(ptr, i, j, &l);
		l.col = l.col1;
		l.delta = (l.col2 - l.col1) / l.count;
	}
	while (l.factor <= 1 && l.count++)
	{
		l.x = l.x0 + l.dx * l.factor;
		l.y = l.y0 + l.dy * l.factor;
		if (k)
		{
			mlx_pixel_put(ptr->scrn, ptr->win, l.x, l.y, l.col);
			l.col += l.delta;
		}
		l.factor += 1. / sqrt((l.dx * l.dx) + (l.dy * l.dy));
	}
	if (k == 0)
		ft_pixel_put(ptr, i, j, 1);
	return (1);
}

int		ft_draw_line(t_mlx *ptr, int i)
{
	int		j;

	if (ptr->tab[i].x > 0)
	{
		if ((ft_pixel_put(ptr, i - 1, i, 0)) == -1)
			return (-1);
	}
	if (ptr->tab[i].y > 0)
	{
		j = ft_twin_index(ptr->tab, i);
		if ((ft_pixel_put(ptr, i, j, 0)) == -1)
			return (-1);
	}
	return (1);
}
