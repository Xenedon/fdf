/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: csellier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/06 13:55:07 by csellier          #+#    #+#             */
/*   Updated: 2016/02/10 14:31:15 by csellier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_pix	ft_rotate(t_var *var, t_mlx *ptr, int i)
{
	t_pix	pix;

	pix = ft_rotatex(var, ptr, i);
	return (pix);
}

t_pix	ft_rotatez(t_var *var, t_pix pix)
{
	t_pix	z;

	z.z = pix.z;
	z.x = pix.x * cos(var->oz) - pix.y * sin(var->oz);
	z.y = pix.x * sin(var->oz) + pix.y * cos(var->oz);
	return (z);
}

t_pix	ft_rotatex(t_var *var, t_mlx *ptr, int i)
{
	t_pix	pixel;

	pixel.x = ptr->tab[i].x - ptr->xmax / 2;
	pixel.y = (ptr->tab[i].y - ptr->ymax / 2) * cos(var->ox)
		- (ptr->tab[i].z * var->alt - ptr->deltaz) / 2 * sin(var->ox);
	pixel.z = ((ptr->tab[i].y - ptr->ymax / 2) * sin(var->ox)
			+ ptr->tab[i].z / 2 * var->alt * cos(var->ox));
	pixel = ft_rotatey(var, pixel);
	return (pixel);
}

t_pix	ft_rotatey(t_var *var, t_pix pix)
{
	t_pix	pixou;

	pixou.y = pix.y;
	pixou.x = pix.x * cos(var->oy) + pix.z * sin(var->oy);
	pixou.z = (pix.z * cos(var->oy) - pix.x * sin(var->oy));
	pixou = ft_rotatez(var, pixou);
	return (pixou);
}
