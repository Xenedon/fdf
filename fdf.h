/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: csellier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 19:31:56 by csellier          #+#    #+#             */
/*   Updated: 2016/02/10 14:31:19 by csellier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "libft/libft.h"
# include "mlx.h"
# include "X.h"
# include <fcntl.h>
# include <math.h>
# include <stdlib.h>

# define LARGEUR 1000
# define HAUTEUR 800

typedef struct	s_pix
{
	double		x;
	double		y;
	double		z;
	double		xt;
	double		yt;
}				t_pix;

typedef struct	s_mlx
{
	void		*scrn;
	void		*win;
	t_pix		*tab;
	int			xmax;
	int			ymax;
	int			deltaz;
	int			zmin;
	int			zmax;
}				t_mlx;

typedef struct	s_line
{
	int			dx;
	int			dy;
	int			x0;
	int			y0;
	int			x;
	int			y;
	double		factor;
	int			count;
	double		col;
	double		col1;
	double		col2;
	double		delta;
}				t_line;

typedef struct	s_var
{
	int			x;
	int			y;
	int			proj;
	int			zoom;
	int			alt;
	double		oz;
	double		ox;
	double		oy;
}				t_var;

typedef struct	s_easy
{
	int			fd;
	int			x;
	int			y;
	char		*buffer;
	char		**split;
}				t_easy;

void			ft_colour(t_mlx *ptr, int i, int j, t_line *l);

t_pix			ft_rotatey(t_var *var, t_pix pix);
t_pix			ft_rotatex(t_var *var, t_mlx *ptr, int i);
t_pix			ft_rotatez(t_var *var, t_pix pix);
t_pix			ft_rotate(t_var *var, t_mlx *ptr, int i);

int				ft_rotation(double a, int keycode, t_var *var);
int				ft_translation(t_var *var, int code);
int				ft_altitude(t_var *var, int keycode, t_pix *tab);
int				ft_zoom(t_var *var, int keycode);
int				ft_transformer(t_mlx *ptr, t_var *var);

int				ft_abs(int i);
int				ft_delta_z(t_mlx *ptr);
int				ft_structmlx(t_mlx *ptr, char *chaine);

int				ft_ntm(int keycode, t_mlx *ptr);
t_var			*ft_keyup(int keycode, t_mlx *ptr, int modif);
int				ft_keyhook(t_mlx *ptr);

void			ft_fill_pix(t_mlx *ptr, int i, int j, t_line *l);
int				ft_twin_index(t_pix *tab, int i);
t_line			*ft_angle(t_mlx *ptr, int i, int j);
int				ft_pixel_put(t_mlx *ptr, int i, int j, int k);
int				ft_draw_line(t_mlx *ptr, int i);

void			ft_free_tab(char **chaine, t_pix *tab);
int				ft_fill_tab(char *file, t_pix *tab, int index);
short int		ft_size(char *chaine);
int				ft_char(char *fichier);
t_pix			*ft_check_file(char *chaine);

void			ft_exit(void);
int				ft_size_window(char *chaine, int largeur);
int				ft_showme(t_mlx *ptr);
void			*ft_reveal(t_mlx *ptr);
t_mlx			*ft_manage_screen(t_pix *tab);

#endif
