/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_keyhook.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: csellier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:44:58 by csellier          #+#    #+#             */
/*   Updated: 2016/02/10 14:28:57 by csellier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		ft_keyhook(t_mlx *ptr)
{
	mlx_do_key_autorepeaton(ptr->scrn);
	mlx_hook(ptr->win, KeyPress, KeyPressMask, ft_ntm, ptr);
	mlx_loop(ptr->scrn);
	return (1);
}

int		ft_ntm(int keycode, t_mlx *ptr)
{
	ft_keyup(keycode, ptr, 1);
	return (1);
}

t_var	*ft_keyup(int keycode, t_mlx *ptr, int modif)
{
	static t_var	var = {0, 0, 0, 0, 1, 0, 0, 0};

	if (modif)
	{
		if (keycode == 53)
			exit(EXIT_SUCCESS);
		if (keycode == 123 || keycode == 124 || keycode == 125 ||
				keycode == 126)
			ft_translation(&var, keycode);
		if (keycode == 69 || keycode == 78)
			ft_zoom(&var, keycode);
		if (keycode == 12 || keycode == 13)
			ft_altitude(&var, keycode, ptr->tab);
		if (keycode == 6 || keycode == 7 || keycode == 16)
			ft_rotation(M_PI / 16, keycode, &var);
		ft_transformer(ptr, &var);
		ft_showme(ptr);
	}
	return (&var);
}
