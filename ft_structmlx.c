/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_structmlx.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: csellier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:24:23 by csellier          #+#    #+#             */
/*   Updated: 2016/02/10 10:24:23 by csellier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		ft_structmlx(t_mlx *ptr, char *chaine)
{
	if (ptr == NULL || chaine == NULL)
		return (-1);
	ptr->xmax = ft_size_window(chaine, 1);
	ptr->ymax = ft_size_window(chaine, 0);
	ptr->deltaz = ft_delta_z(ptr);
	return (1);
}

int		ft_abs(int i)
{
	if (i < 0)
		return (-i);
	return (i);
}

int		ft_delta_z(t_mlx *ptr)
{
	int i;
	int	zmin;
	int	zmax;

	i = 0;
	zmin = 0;
	zmax = 0;
	while (ptr->tab[i].x != -32000)
	{
		if (ptr->tab[i].z > zmax)
			zmax = ptr->tab[i].z;
		if (ptr->tab[i].z < zmin)
			zmin = ptr->tab[i].z;
		i++;
	}
	ptr->zmin = zmin;
	ptr->zmax = zmax;
	if ((zmax - zmin) == 0)
		return (1);
	return (ft_abs(zmax - zmin));
}
