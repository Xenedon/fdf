/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: csellier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 16:25:00 by csellier          #+#    #+#             */
/*   Updated: 2016/02/10 14:21:20 by csellier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		main(int argc, char **argv)
{
	t_pix	*tab;
	t_mlx	*ptr;

	if (argc != 2 || (tab = ft_check_file(argv[1])) == NULL)
	{
		ft_putstr("error\n");
		return (-1);
	}
	if ((ptr = ft_manage_screen(tab)) == NULL)
	{
		ft_putstr("error\n");
		return (-1);
	}
	if (ft_structmlx(ptr, argv[1]) == -1)
	{
		ft_putstr("error\n");
		return (-1);
	}
	if (ft_reveal(ptr) == NULL || ft_keyhook(ptr) == -1)
		return (-1);
	return (0);
}

int		ft_size_window(char *chaine, int largeur)
{
	int		i;
	int		largmax;
	int		fd;
	char	*buf;
	char	**split;

	i = 0;
	largmax = 0;
	if (chaine == NULL || (fd = open(chaine, O_RDONLY)) == -1)
		return (-1);
	while (get_next_line(fd, &buf) == 1)
	{
		if ((split = ft_strsplit(buf, ' ')) == NULL)
			return (-1);
		if (ft_tablen(split) > largmax)
			largmax = ft_tablen(split);
		ft_free_tab(split, NULL);
		free(buf);
		i++;
	}
	if (close(fd) == -1)
		return (-1);
	if (largeur == 1)
		return (largmax);
	return (i);
}

t_mlx	*ft_manage_screen(t_pix *tab)
{
	t_mlx	*ptr;

	if (tab == NULL)
		return (NULL);
	if ((ptr = (t_mlx *)malloc(sizeof(t_mlx))) == NULL)
		return (NULL);
	if ((ptr->scrn = mlx_init()) == NULL)
		return (NULL);
	if ((ptr->win = mlx_new_window(ptr->scrn, LARGEUR, HAUTEUR, "FDF")) == NULL)
		return (NULL);
	if ((ptr->tab = tab) == NULL)
		return (NULL);
	return (ptr);
}

void	*ft_reveal(t_mlx *ptr)
{
	int	projection;
	int	i;

	if (ptr == NULL)
		return (NULL);
	projection = 0;
	i = 1;
	if ((ft_transformer(ptr, ft_keyup(0, NULL, 0)) == -1))
		return (NULL);
	ft_showme(ptr);
	return (ptr->scrn);
}

int		ft_showme(t_mlx *ptr)
{
	int	index;

	index = 0;
	mlx_clear_window(ptr->scrn, ptr->win);
	while (ptr->tab[index].x != -32000)
	{
		ft_draw_line(ptr, index);
		index++;
	}
	return (1);
}
