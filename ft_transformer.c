/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_transformer.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: csellier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 14:59:08 by csellier          #+#    #+#             */
/*   Updated: 2016/02/10 14:23:42 by csellier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		ft_transformer(t_mlx *ptr, t_var *var)
{
	int			i;
	int			cte1;
	int			cte2;
	t_pix		pix;

	cte1 = 10 + var->zoom;
	cte2 = 10 + var->zoom;
	i = 0;
	if (ptr->tab == NULL)
		return (-1);
	while (ptr->tab[i].x != -32000 && var->proj == 0)
	{
		pix = ft_rotate(var, ptr, i);
		ptr->tab[i].xt = cte1 * pix.x - cte1 * pix.y + LARGEUR / 3 + var->x;
		ptr->tab[i].yt = cte2 / 2 * pix.x + cte2 / 2 * pix.y + HAUTEUR / 3 +
			var->y - (cte2 / 2 * pix.z);
		i++;
	}
	return (1);
}

int		ft_altitude(t_var *var, int keycode, t_pix *tab)
{
	int	i;

	i = 0;
	(void)tab;
	if (var == NULL || tab == NULL)
		return (-1);
	if (keycode == 12)
	{
		var->alt += 1;
		if (var->alt == 0)
			var->alt = 1;
	}
	else if (keycode == 13)
	{
		var->alt -= 1;
		if (var->alt == 0)
			var->alt = -1;
	}
	return (1);
}

int		ft_zoom(t_var *var, int keycode)
{
	if (var == NULL)
		return (-1);
	if (keycode == 69)
		var->zoom += 2;
	else if (keycode == 78 && var->zoom > -28)
		var->zoom -= 2;
	return (1);
}

int		ft_rotation(double a, int keycode, t_var *var)
{
	if (keycode == 6)
	{
		var->oz += a;
		if (var->oz >= 2 * M_PI)
			var->oz = 0;
	}
	else if (keycode == 7)
	{
		var->ox += a;
		if (var->ox >= 2 * M_PI)
			var->ox = 0;
	}
	else if (keycode == 16)
	{
		var->oy += a;
		if (var->oy >= 2 * M_PI)
			var->oy = 0;
	}
	return (1);
}

int		ft_translation(t_var *var, int code)
{
	if (var == NULL)
		return (-1);
	if (code == 123)
		var->x -= 6;
	else if (code == 124)
		var->x += 6;
	else if (code == 125)
		var->y += 6;
	else if (code == 126)
		var->y -= 6;
	return (1);
}
