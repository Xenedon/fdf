#***************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: csellier <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/27 17:21:51 by csellier          #+#    #+#              #
#    Updated: 2016/02/10 13:41:33 by csellier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf

CC= gcc

FLAGS = -Wall -Werror -Wextra

SRC = main.c ft_check_file.c ft_draw_line.c ft_transformer.c ft_structmlx.c\
	  ft_rotate.c ft_keyhook.c

OBJ = $(SRC:.c=.o)

all : $(NAME)

$(NAME) : $(OBJ)
	make -C libft/
	$(CC) $(FLAGS) -I libft/includes/ -c $(SRC)
	$(CC) -o $(NAME) $(OBJ) -L libft/ -lft -lmlx -framework OpenGL -framework\
		AppKit

clean:
	make -C libft/ clean
	rm -f $(OBJ)

fclean: clean
	make -C libft/ fclean
	rm -rf $(NAME)

re: fclean all

.PHONY: clean fclean


